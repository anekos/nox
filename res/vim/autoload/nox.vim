" 猫かわいい

" Script varibales {{{

let s:nox_tag_completions_file = '/var/tmp/nox-tag-completions'
let s:initialized = 0
let s:cat_separator = '################################################################################'

let s:unite_tag_menu = {
  \   'description' : 'Nox Tags',
  \   'candidates' : {}
  \}

let s:unite_file_menu = {
  \   'description' : 'Nox Files',
  \   'candidates' : {}
  \}

" }}}

" Utils {{{

function! nox#Chomp(s)
  return substitute(a:s, '\n', '', '')
endfunction

function! nox#Now()
  return nox#Chomp(system('LANG=ja_JP.UTF-8 date'))
endfunction

function! nox#BuildQuery(args)
  return join(map(split(a:args, '\s\+'), 'shellescape(v:val)'), ' ')
endfunction

function! nox#GetAttachDir()
  return expand('%:p:r') . '.attach'
endfunction

" }}}

" Init {{{

function! nox#Init()
  if s:initialized
    return
  endif

  let s:initialized = 1

  if !exists('g:unite_source_menu_menus')
    let g:unite_source_menu_menus = {}
  endif

  function s:unite_tag_menu.map(key, value)
    return {'word' : a:key, 'kind' : 'command', 'action__command' : s:tag_menu_command . ' ' . a:value}
  endfunction

  function s:unite_file_menu.map(key, value)
    return {'word' : a:key, 'kind' : 'file', 'action__path' : a:value}
  endfunction

  call nox#GenerateTagList()
  call unite#custom_source('menu', 'sorters', 'sorter_word')
endfunction

" }}}

" Tag {{{

function! nox#GetTagsFromBuffer()
  let l:result = []
  for l:ln in range(1, line('$'))
    let l:line = getline(l:ln)
    let l:tag = matchstr(l:line, '\(^tag:\s*\)\@<=.*')
    if len(l:tag) > 0
      let l:result = add(l:result, l:tag)
    endif
  endfor
  return l:result
endfunction

" }}}

" For Command {{{

function! nox#AddTag(tag)
  let l:found = 0

  for l:line in range(1, 100)
    if l:found
      if match(getline(l:line), '^tag: ') >= 0
        let l:found = l:line
      else
        break
      endif
    endif
    if match(getline(l:line), '^title: ') >= 0
      let l:found = l:line
    endif
  endfor

  if l:found
    call append(l:found, ['tag: ' . a:tag])
  endif
endfunction


function! nox#NewDiary(title)
  let l:date = nox#Chomp(system('date "+%Y/%m/%d"'))
  let l:filename_base = g:nox_diary_path . '/' . l:date
  let l:existing = split(glob(l:filename_base . '*.nox'), '\n')

  if len(l:existing)
    let l:filename = l:existing[0]
  else
    let l:filename = l:filename_base . (len(a:title) ? '-' . a:title : '') . '.nox'
  endif

  let s:next_title = l:date . ' - ' . a:title

  echomsg l:filename

  execute 'edit' escape(l:filename, ' ')

  if len(a:title)
    if len(l:existing)
      call append('$', ['', ''])
    endif
    call append('$', ['# ' . a:title, '', ''])
    normal G
    startinsert
  endif

  unlet s:next_title
endfunction

function! nox#New()
  let l:nox_new_entry = {
        \ 'is_selectable' : 1,
        \ }

  function! nox_new_entry.func(candidates)
    if len(a:candidates) < 1
      return
    endif
    let l:filename = input('title: ')
    if len(l:filename)
      let l:path = a:candidates[0].word . '/' . escape(l:filename, ' \') . '.nox'
      execute 'edit' l:path
    endif
  endfunction

  call unite#custom#action('directory', 'nox_new_entry', nox_new_entry)
  unlet l:nox_new_entry

  let l:source = 'directory_rec/async:' . g:nox_document_path
  execute 'Unite' l:source '-default-action=nox_new_entry'
endfunction


function! nox#Search(query)
  let l:args = nox#BuildQuery(a:query)
  let l:files = split(system(join(['NOX_DOCUMENT=' . g:nox_document_path, 'nox-search', l:args])), "\n")
  call nox#OpenUniteFileMenu(l:files)
endfunction


function! nox#Cat(query)
  let l:args = nox#BuildQuery(a:query)
  let l:cmd = join(['NOX_DOCUMENT=' . g:nox_document_path, 'nox-search', l:args, '|', 'NOX_DOCUMENT=' . g:nox_document_path, 'nox-cat'])
  echomsg l:cmd
  let l:content = split(system(l:cmd), "\n")
  enew
  let b:initialized = 1
  call append(0, l:content)
  setlocal filetype=nox
  setlocal nomodifiable
  setlocal nomodified
  set filetype=nox-cat
  1
endfunction


function! nox#OpenCurrentSectionFile(command)
  let l:previous = 0
  let l:start = getline('.') == s:cat_separator ? min([line('.') + 2, line('$')]) : line('.')
  let l:pos = getcurpos()
  let [l:bufnum, l:lnum, l:col, l:off, l:curswant] = getcurpos()
  for l:current in range(l:start, 1, -1)
    let l:line = getline(l:current)
    if l:previous && match(l:line, '^## ') == 0
      let l:filepath = substitute(l:line, '^## ', '', '')
      execute (a:command) escape(l:filepath . '.nox', ' ')
      call setpos('.', [0, l:start - l:current - 1, l:col, l:off])
      normal zz
      return
    endif
    let l:previous = l:line == s:cat_separator
  endfor
endfunction


function! nox#Attach(source)
  let l:is_url = match(a:source, '://') >= 0

  let l:name = input('Please input filename: ')
  if l:name == '' | return | endif

  let l:dir = nox#GetAttachDir()
  if empty(glob(fnameescape(l:dir)))
    if mkdir(l:dir) != 1
      echoerr 'Could not make directory: ' . l:dir
      return
    endif
  endif

  let l:output_arg = fnameescape(l:dir . '/' . l:name)
  let l:source_arg = fnameescape(a:source)

  echomsg l:source_arg
  if !empty(glob(l:output_arg))
    if input(printf('Overwrite %s ? (yes/no): ', l:output_arg)) != 'yes'
      echoerr 'Canceled!'
      return
    endif
  endif

  if l:is_url
    call system('curl -o ' . l:output_arg . ' ' . l:source_arg)
  else
    call system('cp --no-clobber ' . l:source_arg . ' ' . l:output_arg)
  endif

  call append('.', printf('[%s](%s)', l:name, a:source))

  echo 'Done'
endfunction


function! nox#OpenReference()
  let l:col = col('.')
  let l:attr = synIDattr(synID(line('.'), l:col, 1), 'name')

  if index(['mkdLink', 'markdownLinkText'], l:attr) >= 0
    let l:line = getline('.')

    let l:left_part = matchstr(l:line[: l:col - 1], '.*\[')
    let l:right_pos = match(l:line[l:col - 1 :], '\]')

    let l:name = l:line[len(l:left_part) : l:col + l:right_pos - 2]
    let l:path = nox#GetAttachDir() . '/' . l:name

    call system('rifle ' . fnameescape(fnamemodify(l:path, ':p')))
  else
    if exists(':OpenBrowserSmartSearch') == 2
      execute 'normal' "\<Plug>(openbrowser-smart-search)"
    endif
  endif
endfunction

" }}}

" Auto Command {{{

function! nox#UpdateDate()
  if exists('b:new_file') && b:new_file
    " DO NOTHING
  else
    let l:save_cursor = getcurpos()
    silent! execute '%s/^updated-at: \zs.*/' . nox#Now() . '/'
    call setpos('.', l:save_cursor)
  endif
endfunction


function! nox#InitBuffer()
  if exists('b:initialized') && b:initialized | return | endif

  if getfsize(bufname('')) < 10
    let l:date = nox#Now()
    if exists('s:next_title')
      let l:title = 'title: ' . s:next_title
    else
      let l:title = 'title: ' . expand('%:t:r')
    endif
    let l:created_at = 'created-at: ' . date
    let l:updated_at = 'updated-at: ' . date

    let l:filepath = expand('%:p:r')
    if len(l:filepath) > 0 && exists('g:NoxTagFromPath')
      let l:lines = [l:title]
      let l:tag = g:NoxTagFromPath(l:filepath)
      if len(l:tag) > 0
        let l:lines = add(l:lines, 'tag: ' . join(l:tag, ' -> '))
      endif
      let l:lines = l:lines + [l:created_at, l:updated_at, '', '']
      call append(line('.'), l:lines)
    else
      call append(line('.'), [l:title, l:created_at, l:updated_at, '', ''])
    endif

    let b:new_file = 1
  endif

  let b:current_tags = nox#GetTagsFromBuffer()

  normal zzG
endfunction


function! nox#OnUpdateBuffer()
  call nox#UpdateDate()
  let l:current_tags = nox#GetTagsFromBuffer()
  if l:current_tags != b:current_tags
    let b:current_tags = l:current_tags
    call nox#GenerateTagList()
  endif
endfunction

" }}}

" Unite {{{

function! nox#GenerateTagList()
  call system(join([g:nox_tag_command, g:nox_document_path, ' > ' . s:nox_tag_completions_file], ' '))

  let s:unite_tag_menu.candidates = {}
  for l:tag in readfile(s:nox_tag_completions_file)
    let s:unite_tag_menu.candidates[l:tag] = l:tag
  endfor

  let g:unite_source_menu_menus['nox-tag'] = deepcopy(s:unite_tag_menu)
endfunction


function! nox#OpenUniteTagMenu(command)
  call nox#Init()
  let s:tag_menu_command = a:command
  Unite -immediately menu:nox-tag
endfunction


function! nox#OpenUniteFileMenu(files)
  call nox#Init()

  let s:unite_file_menu.candidates = {}
  for l:file in a:files
    let l:key = fnamemodify(l:file[len(g:nox_document_path) :], ':r')
    let s:unite_file_menu.candidates[l:key] = l:file
  endfor

  let g:unite_source_menu_menus['nox-file'] = deepcopy(s:unite_file_menu)

  Unite -immediately menu:nox-file
endfunction

" }}}

" Syntax {{{

function! nox#SyntaxInit()
  for l:name in ['mkd.vim', 'markdown.vim']
    let l:paths = split(globpath(&runtimepath, 'syntax/' . l:name), '\n')
    if len(l:paths) > 0
      execute 'source ' . l:paths[0]
      return
    endif
  endfor
endfunction

" }}}

" Cleaner {{{

function! nox#CleanUrlAndTitle()
  for l:ln in range(1, line('$'))
    let l:line = getline(l:ln)
    let l:replaced = substitute(l:line, '\(https\=://www.amazon.[^/]\+/\)\([^/]*/\)\=[^/]*/\(dp/[^/]\+/\=\).*', '\1\3', 'g')
    let l:replaced = substitute(l:replaced, '\(https\=://www.amazon.[^/]\+/\)\([^/]*/\)\=\(gp/product/[^/]\+/\=\).*', '\1\3', 'g')
    let l:replaced = substitute(l:replaced, 'Amazon..\{-}： *\(.\{-}\): [^ ]\+', '\1', 'g')
    if l:line != l:replaced | call setline(l:ln, l:replaced) | endif
  endfor
endfunction

" }}}
