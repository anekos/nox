
augroup nox
  autocmd BufWritePre *.nox :call nox#OnUpdateBuffer()
augroup END

command! -nargs=* NoxTagAdd call nox#AddTag(<q-args>)
command! -nargs=* NoxDiary call nox#NewDiary(<q-args>)
command! -nargs=0 NoxTagUpdate call nox#GenerateTagList()
command! -nargs=0 NoxNew call nox#New()
command! -nargs=0 NoxInit call nox#Init()
command! -nargs=* NoxSearch call nox#Search(<q-args>)
command! -nargs=* NoxCat call nox#Cat(<q-args>)
command! -nargs=* NoxUnugly call nox#CleanUrlAndTitle()
command! -nargs=* NoxAttach call nox#Attach(<q-args>)
command! -nargs=* NoxOpenReference call nox#OpenReference()


nnoremap <silent> <Plug>(nox-tag-search-menu) :<C-u>call nox#OpenUniteTagMenu('NoxSearch tag: ')<CR>
nnoremap <silent> <Plug>(nox-cat-menu) :<C-u>call nox#OpenUniteTagMenu('NoxCat tag: ')<CR>

nnoremap <silent> <Plug>(nox-edit-current-section-file) :<C-u>call nox#OpenCurrentSectionFile('edit')<CR>
nnoremap <silent> <Plug>(nox-tabedit-current-section-file) :<C-u>call nox#OpenCurrentSectionFile('tabedit')<CR>
nnoremap <silent> <Plug>(nox-tag-add-menu) :<C-u>call nox#OpenUniteTagMenu('NoxTagAdd')<CR>


if !exists('g:unite_source_outline_info.markdown')
  let g:unite_source_outline_info = {}
endif

let g:unite_source_outline_info.nox = {
\ 'heading': '^#',
\}

let g:unite_source_outline_info['nox-cat'] = {
\ 'heading': '^#',
\}
