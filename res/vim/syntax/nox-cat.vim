
if exists("b:current_syntax")
  unlet! b:current_syntax
endif

execute 'source ' . split(globpath(&runtimepath, 'syntax/markdown.vim'), '\n')[0]
